cnt = 0

def dec_defcounter(func):
    def wrapper():
        func()
        global cnt 
        cnt += 1
    return wrapper

@dec_defcounter
def test(): 
    pass

test() 
test() 
test() 
test()

print('Всего вызвано функций: ', cnt)
