import datetime

def date(day: int, month: int, year: int):
    try:
        datetime.datetime(year, month, day)
        return True
    except:
        return False


print(date(1, 1, 2001))
print(date(29, 2, 2658701))
