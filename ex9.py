def my_func(num):
    res = []
    for i in num:
        if i not in res:
            res.append(i)
    return tuple(res[::-1])

a = [5,67,7,2,5,2,345,345,5,2,1]

print(my_func(a))
