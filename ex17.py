class Pet:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def show(self):
        print(f"Меня зовут {self.name}, мой возраст {self.age} лет ")

pet1 = Pet("Барсик", 4)
pet1.show()
pet2 = Pet("Вискас", 5)
pet2.show()