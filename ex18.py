class User():
    def __init__ (self, name, age, status):
        self.name = name
        self.age = age
        self.status = status

    def show_details(self):
        print("Детали")
        print (" ")
        print("Имя", self.name)
        print("Возраст", self.age)
        print("Статус", self.status)

class Bank(User):
    def __init__(self, name, age, status):
        super().__init__(name, age, status)
        self.balance = 0

    def deposit(self, amount):
        self.amount = amount
        self.balance = self.balance + self.amount
        print("Баланс был обновлен : ", self.balance)
    
    def view_balance(self):
        self.show_details()
        print("Баланс аккаунта: ", self.balance)
johan = Bank("Johan", 20, "gold")
johan.deposit(1000)
johan.view_balance()